package ru.t1.dkononov.tm.api.service;

import ru.t1.dkononov.tm.entity.dto.TaskDto;

import java.util.List;

public interface TaskDtoService {
    List<TaskDto> findAll();

    TaskDto save(TaskDto task);

    TaskDto save();

    TaskDto findById(String id);

    boolean exsistsById(String id);

    long count();

    void deleteById(String id);

    void deleteById(TaskDto task);

    void deleteById(List<TaskDto> tasks);

    void clear();
}
