package ru.t1.dkononov.tm.api.endpoint;

import ru.t1.dkononov.tm.entity.dto.TaskDto;

import java.util.List;

public interface TaskRestEndpoint {
    List<TaskDto> findAll();

    TaskDto save(TaskDto task);

    TaskDto findById(String id);

    boolean exsistsById(String id);

    long count();

    void deleteById(String id);

    void deleteById(TaskDto task);

    void deleteById(List<TaskDto> tasks);

    void clear();
}
