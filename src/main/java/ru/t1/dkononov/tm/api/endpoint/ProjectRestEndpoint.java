package ru.t1.dkononov.tm.api.endpoint;

import ru.t1.dkononov.tm.entity.dto.ProjectDto;

import java.util.List;

public interface ProjectRestEndpoint {
    List<ProjectDto> findAll();

    ProjectDto save(ProjectDto project);

    ProjectDto findById(String id);

    boolean exsistsById(String id);

    long count();

    void deleteById(String id);

    void deleteById(ProjectDto project);

    void deleteById(List<ProjectDto> projects);

    void clear();
}
