package ru.t1.dkononov.tm.entity.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.dkononov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class TaskDto {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    //    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart = new Date();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    private String projectId;

    public TaskDto(final String name) {
        this.name = name;
    }

}
