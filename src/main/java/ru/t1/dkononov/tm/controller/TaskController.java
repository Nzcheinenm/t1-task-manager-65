package ru.t1.dkononov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.dkononov.tm.api.service.ProjectDtoService;
import ru.t1.dkononov.tm.api.service.TaskDtoService;
import ru.t1.dkononov.tm.entity.dto.ProjectDto;
import ru.t1.dkononov.tm.entity.dto.TaskDto;
import ru.t1.dkononov.tm.enumerated.Status;

import java.time.LocalDate;
import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskDtoService service;

    @Autowired
    private ProjectDtoService projectService;

    @GetMapping("/task/create")
    public String create() {
        service.save();
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        service.deleteById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") TaskDto task,
            BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        service.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final TaskDto task = service.findById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects());
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    private Collection<ProjectDto> getProjects() {
        return projectService.findAll();
    }

    private Status[] getStatuses() {
        return Status.values();
    }

}
