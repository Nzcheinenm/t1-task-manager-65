package ru.t1.dkononov.tm;

import org.checkerframework.checker.units.qual.C;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkononov.tm.client.ProjectRestEndpointClient;
import ru.t1.dkononov.tm.entity.dto.ProjectDto;
import ru.t1.dkononov.tm.marker.IntegrationCategory;

import java.util.List;

public class ProjectRestEndpointTest {

    final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    final ProjectDto projectDto1 = new ProjectDto("Test 1");

    final ProjectDto projectDto2 = new ProjectDto("Test 2");

    final ProjectDto projectDto3 = new ProjectDto("Test 3");

    final ProjectDto projectDto4 = new ProjectDto("Test 4");
/*
    @Before
    public void initTest() {
        client.save(projectDto1);
        client.save(projectDto2);
        client.save(projectDto3);
    }

    @After
    public void afterTest() {
        final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();
        client.deleteById(projectDto1);
        client.deleteById(projectDto2);
        client.deleteById(projectDto3);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        final String expected = projectDto4.getName();
        final ProjectDto projectDto = client.save(projectDto4);
        Assert.assertNotNull(projectDto);
        final String actual = projectDto.getName();
        Assert.assertEquals(expected,actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeById() {
        final String id = projectDto1.getId();
        client.deleteById(id);
        Assert.assertNull(client.findById(id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        final List<ProjectDto> projects = client.findAll();
        Assert.assertTrue(projects.contains(projectDto3));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void count() {
        final long count = client.count();
        Assert.assertTrue(count > 0);
    }
*/
}
